program GfxLog;

uses
  Vcl.Forms,
  UFmMain in 'UFmMain.pas' {fmMain},
  UGfxLog in 'UGfxLog.pas',
  UDebug in 'UDebug.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmMain, fmMain);
  Application.Run;
end.
