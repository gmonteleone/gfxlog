unit UFmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  UGfxLog, Vcl.ExtCtrls, Vcl.StdCtrls;

const
  KVersion = 'V1.0.7';

type
  TfmMain = class(TForm)
    eFilename: TEdit;
    btnLoad: TButton;
    OpenDialog1: TOpenDialog;
    PaintBox1: TPaintBox;
    sbHorizontal: TScrollBar;
    eFilter: TEdit;
    Label1: TLabel;
    cbFilterInversion: TCheckBox;
    cbCaseInsensitive: TCheckBox;
    cbGroup: TCheckBox;
    procedure btnLoadClick(Sender: TObject);
    procedure cbCaseInsensitiveClick(Sender: TObject);
    procedure cbFilterInversionClick(Sender: TObject);
    procedure cbGroupClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta:
        Integer; MousePos: TPoint; var Handled: Boolean);
    procedure PaintBox1Paint(Sender: TObject);
    procedure sbHorizontalChange(Sender: TObject);
    procedure eFilterKeyPress(Sender: TObject; var Key: Char);
    procedure PaintBox1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    FGfxLog: TGFxLog;
    FFile: TStringList;
    FGrouping: Boolean;
    { Private declarations }
    procedure AdjustScroll;
    procedure LoadFile(AName: string);
  public
    { Public declarations }
  end;

var
  fmMain: TfmMain;

implementation

uses
  UDebug,
  RegularExpressions,
  StrUtils;

{$R *.dfm}

procedure TfmMain.btnLoadClick(Sender: TObject);
begin
  OpenDialog1.InitialDir := ExtractFilePath(Application.ExeName);
  if OpenDialog1.Execute then begin
    eFilename.Text := OpenDialog1.FileName;
    LoadFile(eFilename.Text);
  end;
end;

procedure TfmMain.FormDestroy(Sender: TObject);
begin
  FGfxLog.Free;
  FFile.Free;
end;

procedure TfmMain.FormCreate(Sender: TObject);
begin
  SiInit;
  Caption := Caption + ' ' + KVersion;
  FFile := TStringList.Create;
  FGfxLog := TGfxLog.Create;
  PaintBox1.Canvas.Brush.Color := clWhite;
  PaintBox1.Canvas.Rectangle(PaintBox1.ClientRect);
  FGfxLog.Canvas := PaintBox1.Canvas;
  FGfxLog.Size := PaintBox1.ClientRect;
end;

procedure TfmMain.AdjustScroll;
var
  LMax: Integer;
begin
  LMax := FGfxLog.EntryCount;
  if (LMax > 0) and (LMax <> sbHorizontal.Max) then
    sbHorizontal.Max := LMax;
end;

procedure TfmMain.cbCaseInsensitiveClick(Sender: TObject);
begin
  if (eFilename.Text <> '') and (Trim(eFilter.Text) <> '') then
    LoadFile(eFilename.Text);
end;

procedure TfmMain.cbFilterInversionClick(Sender: TObject);
begin
  if (eFilename.Text <> '') and (Trim(eFilter.Text) <> '') and not FGrouping then
    LoadFile(eFilename.Text);
end;

procedure TfmMain.cbGroupClick(Sender: TObject);
begin
  if cbGroup.Checked then begin
    FGrouping := true;
    cbFilterInversion.Checked := False;
    cbFilterInversion.Enabled := False;
  end else begin
    cbFilterInversion.Enabled := True;
    FGrouping := False;
  end;
  if (eFilename.Text <> '') and (Trim(eFilter.Text) <> '') then
    LoadFile(eFilename.Text);
end;

procedure TfmMain.eFilterKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) and (eFilename.Text <> '') then
    LoadFile(eFilename.Text);
end;

procedure TfmMain.FormMouseWheel(Sender: TObject; Shift: TShiftState;
    WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  LNewPosition: Integer;
  LDelta: Integer;
  LDbgStr: string;

  procedure weelScroll;
  begin
    if WheelDelta < 0 then
      LDelta := -LDelta;

    LNewPosition := sbHorizontal.Position + LDelta;

    if LNewPosition >= sbHorizontal.Max then
      LNewPosition := sbHorizontal.Max;

    if LNewPosition < 0 then
      LNewPosition := 0;

    sbHorizontal.Position := LNewPosition;
  end;

  procedure weelZoom;
  var
    LZoom: Integer;
  begin
    SiLog(dlvDebug,'Zoom');
    LZoom := FGfxLog.Zoom;
    if WheelDelta >= 0 then
      Inc(LZoom)
    else if LZoom > 0 then
      Dec(LZoom);
    FGfxLog.Zoom := LZoom;
    sbHorizontal.Position := FGfxLog.Offset;
  end;

begin
  if ssShift in Shift then
    weelZoom
  else if ssCtrl in Shift then begin
    LDelta := sbHorizontal.Max div 100;
    weelScroll;
  end else begin
    LDelta := 1;
    weelScroll;
  end;

  Handled := True;
  PaintBox1.Invalidate;
end;


procedure TfmMain.FormResize(Sender: TObject);
begin
  FGfxLog.Repaint;
  sbHorizontal.Position := FGfxLog.Offset;
end;

procedure TfmMain.LoadFile(AName: string);
var
  I: Integer;
  LHeader: string;
  LIndex: Integer;
  LLenHeader: Integer;
  LString: string;
  LRegEx: TRegEx;
  LFilter: string;
  LIsMatch: Boolean;
  LMatch: TMatch;
begin
  if (AName <> '') and FileExists(AName) then begin
    FFile.Clear;
    FFile.LoadFromFile(eFilename.Text);
    FGfxLog.Clear;
    LIndex := 0;
    LFilter := Trim(eFilter.Text);
    if LFilter <> '' then begin
      if cbCaseInsensitive.Checked then
        LRegEx := TRegEx.Create(LFilter,[roIgnoreCase])
      else
        LRegEx := TRegEx.Create(LFilter);
    end;

    for I := 0 to FFile.Count-1 do begin
      { TODO 2: gestione del tempo }
      LString := FFile.Strings[i];

      LIsMatch := True;
      if (LFilter <> '') then begin
        { check if RegExp match string }
        LIsMatch := LRegEx.IsMatch(LString);
        { if filter inversion active invert match status }
        if cbFilterInversion.Checked then
          LIsMatch := not LIsMatch;

        if FGrouping then begin
          LMatch := LRegEx.Match(LString);
          { the group 0 is full matched string,
            group starting form 1 is matched inside () }
          if LMatch.Groups.Count > 1 then
            LString := LMatch.Groups.Item[1].Value
          else
            LString := LMatch.Value;
        end;
      end;

      if LIsMatch then begin
        FGfxLog.Add(LString,LIndex);
        Inc(LIndex);
      end;
    end;
    AdjustScroll;
    PaintBox1.Invalidate;
  end;
end;

procedure TfmMain.PaintBox1Click(Sender: TObject);
var
  LPoint: TPoint;
begin
  LPoint := Mouse.CursorPos;
  LPoint := PaintBox1.ScreenToClient(LPoint);
  FGfxLog.ShowName(LPoint.Y);
end;

procedure TfmMain.PaintBox1Paint(Sender: TObject);
var
  LMax: Integer;
begin
  FGfxLog.Size := PaintBox1.ClientRect;
  AdjustScroll;
  { should be the only call to repaint }
  FGfxLog.Repaint;
end;

procedure TfmMain.sbHorizontalChange(Sender: TObject);
begin
  if FGfxLog.Offset <> sbHorizontal.Position then begin
    FGfxLog.Offset := sbHorizontal.Position;
    PaintBox1.Invalidate;
  end;
end;

end.
