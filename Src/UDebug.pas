unit UDebug;

interface
  uses
{$IFDEF USE_SMARTINSPECT}
    SmartInspect,
    SiAuto,
    Db,
{$ENDIF}
{$IFDEF USE_CODESITE}
    CodeSiteLogging,
{$ENDIF}
    Classes,
    SysUtils;

const
{$IFDEF USE_SMARTINSPECT}
SiDebugId = 'SmartInspect'
kFileExt = '.sic'
{$DEFINE TRACE_ON}
{$ENDIF}

{$IFDEF USE_CODESITE}
SiDebugId = 'Codesite';
kFileExt = '.codesite';
{$DEFINE TRACE_ON}
{$ENDIF}

{$IFNDEF TRACE_ON}
SiDebugId = '';
{$ENDIF}

type
  TDbgLevel = (
    dlvError,
    dlvWarning,
    dlvMessage,
    dlvVerbose,
    dlvDebug,
    dlvDebugLow
  );

function SiInit: string;
procedure SiLog(ALevel: TDbgLevel; AString: string); overload;
procedure SiLog(ALevel: TDbgLevel; AString: string; AArgs: array of const);
    overload;
procedure SiLogEnter(AObject: TObject; ATitle: string);
procedure SiLogLeave(AObject: TObject; ATitle: string);
procedure SiLogException(E: Exception; ATitle: string = '');

implementation

uses
  Windows,
  Forms;

function SiInit: string;
var
  LFilename: string;
  LFound: Boolean;
begin
{$IFDEF TRACE_ON}
	LFileName := ChangeFileExt(Application.ExeName,kFileExt);
	LFound := false;
	if FileExists(LFileName) then
		LFound := true;
{$ENDIF}
{$IFDEF USE_SMARTINSPECT}
	if not LFound begin
		LFileName := 'C:\' + ExtractFileName(LFileName);
		if FileExists(LFileName) then
			LFound := true;
	end;

	if LFound then begin
		Si.LoadConfiguration(LFileName);
    Application.OnException := SiMain.ExceptionHandler;
  end;
{$ENDIF}
{$IFDEF USE_CODESITE}
  CodeSite.Enabled := LFound;
{$ENDIF}
  Result := SiDebugId;
end;

procedure SiLog(ALevel: TDbgLevel; AString: string);
begin
{$IFDEF USE_SMARTINSPECT}
  case ALevel of
    dlvError: SiMain.LogError(AString);
    dlvWarning: SiMain.LogWarning(AString);
    dlvMessage: SiMain.LogMessage(AString);
    dlvVerbose: SiMain.LogVerbose(AString);
    dlvDebug,
    dlvDebugLow: SiMain.LogDebug(AString);
  end;
{$ENDIF}
{$IFDEF USE_CODESITE}
  case ALevel of
    dlvError: CodeSite.SendError(AString);
    dlvWarning: CodeSite.SendWarning(AString);
    dlvMessage: CodeSite.Send(AString);
    dlvVerbose: CodeSite.SendNote(AString);
    dlvDebug: CodeSite.Send(csmGreen,AString);
    dlvDebugLow: CodeSite.Send(csmViolet,AString);
  end;
{$ENDIF}
end;

procedure SiLog(ALevel: TDbgLevel; AString: string; AArgs: array of const);
var
  LFormatSettings: TFormatSettings;
  LString: String;
begin
  { need delphi 2011 or higger }
  LFormatSettings := TFormatSettings.Create(GetThreadLocale);
  LString := Format(AString,AArgs,LFormatSettings);
  SiLog(ALevel,LString);
end;

procedure SiLogEnter(AObject: TObject; ATitle: string);
begin
{$IFDEF USE_SMARTINSPECT}
  SiMain.EnterMethod(AObject,ATitle);
{$ENDIF}
{$IFDEF USE_CODESITE}
  CodeSite.EnterMethod(AObject,ATitle);
{$ENDIF}
end;

procedure SiLogLeave(AObject: TObject; ATitle: string);
begin
{$IFDEF USE_SMARTINSPECT}
  SiMain.LeaveMethod(AObject,ATitle);
{$ENDIF}
{$IFDEF USE_CODESITE}
  CodeSite.ExitMethod(AObject,ATitle);
{$ENDIF}
end;

procedure SiLogException(E: Exception; ATitle: string = '');
begin
{$IFDEF USE_SMARTINSPECT}
  SiMain.LogException(E,ATitle);
{$ENDIF}
{$IFDEF USE_CODESITE}
  CodeSite.SendException(ATitle,E);
{$ENDIF}
end;

end.
