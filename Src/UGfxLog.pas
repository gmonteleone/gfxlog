unit UGfxLog;

interface

uses
  Generics.Collections,
  SysUtils,
  Types,
  Graphics,
  Classes;

const
  KHNameOffset = 50;
  KVNameOffset = 20;
  KMaxZoom = 100;

type
  TGfxLogEntry = record
    NameId: Integer;
    Time: Cardinal;
  end;

  TGfxLog = class
    private
      FNames: TStringList;
      FEntryList: TList<TGfxLogEntry>;
      FCanvas: TCanvas;
      FSize: TRect;
      FVStep: Integer;
      FVOffset: Integer;
      FLastOffet: Integer;       { value of offet prior to a change }
      FOffset: Integer;
      FZoom: Integer;
      FIntZoom: Integer;
      FHNameOffset: Integer;
      FRedIndex: Integer;

      procedure SetCanvas(ACanvas: TCanvas);
      procedure SetSize(ASize: TRect);
      procedure SetOffset(AOffset: Integer);
      procedure SetZoom(AZoom: Integer);
      function GetEntryCount: Integer;
      procedure DrawNames;
      procedure UpdateNameSize(AString: string);

    public
      constructor Create;
      destructor Destroy; override;
      procedure Repaint;
      procedure Clear;
      procedure Add(AName: String; ATime: Cardinal);
      procedure ShowName(AYValue: Integer);
      property Canvas: TCanvas read FCanvas write SetCanvas;
      property Size: TRect read FSize write SetSize;
      property Offset: Integer read FOffset write SetOffset;
      property EntryCount: Integer read GetEntryCount;
      property Zoom: Integer read FZoom write setZoom;
  end;

  EGfxLogError = class(Exception);

implementation

uses
  UDebug;


procedure TGfxLog.SetCanvas(ACanvas: TCanvas);
begin
  if FCanvas <> ACanvas then begin
    FCanvas := ACanvas;
    if FCanvas <> nil then
      Repaint;
  end;
end;

procedure TGfxLog.SetSize(ASize: TRect);
begin
  if FSize <> ASize then begin
    FSize := ASize;
    Repaint;
  end
end;

procedure TGfxLog.SetOffset(AOffset: Integer);
begin
  if FOffset <> AOffset then begin
    SiLog(dlvDebug,'Offset: %d',[AOffset]);
    FLastOffet := FOffset;
    FOffset := AOffset;
    Repaint;
  end;
end;

procedure TGfxLog.SetZoom(AZoom: Integer);
begin
  if FZoom <> AZoom then begin
    FZoom := AZoom;
    if FZoom > (KMaxZoom-1) then
      FZoom := (KMaxZoom-1)
    else if FZoom < 0 then
      FZoom := 0;
    SiLog(dlvDebug,'Zoom %d',[FZoom]);
    Repaint;
  end
end;

function TGfxLog.GetEntryCount: Integer;
begin
  Result := FEntryList.Count;
end;

procedure TGfxLog.DrawNames;
var
  I: Integer;
  LRect: TRect;
  LStr: string;
begin
  if FNames.Count > 0 then begin
    FVStep := FSize.Height div FNames.Count;
    if FVStep > KVNameOffset then
      FVOffset := KVNameOffset
    else
      FVOffset := FVStep;
    LRect.Left   := 0;
    LRect.Width  := FHNameOffset;
    FCanvas.Pen.Style := psDot;
    for I := 0 to FNames.Count-1 do begin
      LRect.Top := I*FVStep;
      LRect.Height := FVOffset;
      LStr := FNames.Strings[I];
      FCanvas.TextRect(LRect,LStr);
      FCanvas.MoveTo(0,LRect.Bottom);
      FCanvas.LineTo(FSize.Width,LRect.Bottom);
    end;
    FCanvas.MoveTo(FHNameOffset,FVStep);
    FCanvas.Pen.Style := psSolid;
  end;
end;

procedure TGfxLog.UpdateNameSize(AString: string);
var
  LStringLen: Integer;
begin
  Assert(FCanvas <> nil,'Invalid Canvas');
  LStringLen := FCanvas.TextExtent(AString).cx;
  if FHNameOffset < LStringLen then
    FHNameOffset := LStringLen;
end;

constructor TGfxLog.Create;
begin
  FRedIndex := -1;
  FNames     := TStringList.Create;
  FEntryList := TList<TGfxLogEntry>.Create;
  FHNameOffset := KHNameOffset;
end;

destructor TGfxLog.Destroy;
begin
  FNames.Free;
  FEntryList.Free;
end;

{ Should Called only in OnPaint event }
procedure TGfxLog.Repaint;
var
  LSpan: Integer;
  LSamplePoints: Integer;
  LWidth: Integer;
  LHStep: Integer;
  X,J,Y: Integer;
  LAdjust: Integer;
  LError: Integer;
  LStep: Integer;
begin
  FCanvas.MoveTo(0,0);
  FCanvas.FillRect(FSize);
  DrawNames;

  { calc number of plotted point: every plotted point is a Entry item
    this is more like a resolution tha a numeber of point in paintbox  }
  LSamplePoints := ((FSize.Width-FHNameOffset) * (KMaxZoom-FZoom)) div KMaxZoom;
  { LSpan: point that we accutally will draw }
  LSpan := LSamplePoints;

  { here we handle end of list }
  if (LSamplePoints+FOffset >  FEntryList.Count) then begin
      LSpan := FEntryList.Count-FOffset;
      { we limit span }
      if (LSpan < (LSamplePoints div 3)) and (FLastOffet < FOffset) then
        FOffset := FLastOffet;
  end;
  SiLog(dlvDebug,'sp: %d/%d, off: %d, count: %d',[LSpan,LSamplePoints,FOffset,FEntryList.Count]);

  { real plot drawing with }
  LWidth := FSize.Width-FHNameOffset;

  if (FNames.Count > 0) then begin
    LHStep := LWidth div LSamplePoints;

    { calculate error due a integer division }
    LError := LWidth - LSamplePoints * LHStep;
    if LHStep <> 0 then begin
        { reset position of first entry (we use MoveTo and not LineTo) }
        FCanvas.MoveTo(LHStep+FHNameOffset,
        FEntryList.Items[FOffset].NameId*FVStep+FVOffset);
        LAdjust := 0;
        for X := 0 to LSpan-1 do begin
          Y := FEntryList.Items[X+FOffset].NameId;
          { draw V Line }
          FCanvas.LineTo(X*LHStep+LAdjust+FHNameOffset,Y*FVStep+FVOffset);
          { change pen width for H Line }
          FCanvas.Pen.Width := 3;

          { LError / LSamplePoints is the reminder of previous integer division
            LAdjust = x * Frac(LHStep)
            LAdjust = x * Lerror / LSamplePoints
            we update LAdjust here because should incrase length of H lines only }
          LAdjust := (X * LError) div LSamplePoints;
          FCanvas.LineTo((X+1)*LHStep+LAdjust+FHNameOffset,Y*FVStep+FVOffset);
          { return V Width }
          FCanvas.Pen.Width := 1;
        end
    end;
  end;
end;

procedure TGfxLog.Clear;
begin
  FHNameOffset := 0;
  FNames.Clear;
  FEntryList.Clear;
end;

procedure TGfxLog.Add(AName: string; ATime: Cardinal);
var
  LEntry: TGfxLogEntry;
  LIndex: Integer;
begin
  { TODO 2 : inserire algoritmo per lista sorted. pi� veloce }
  LIndex := FNames.IndexOf(AName);
  if LIndex < 0 then begin
    LIndex := FNames.Add(AName);
    UpdateNameSize(AName);
  end;

  LEntry.NameId := LIndex;
  LEntry.Time := ATime;
  FEntryList.Add(LEntry);
end;

procedure TGfxLog.ShowName(AYValue: Integer);
var
  LStr: string;
  LRect: TRect;
begin
  if FNames.Count > 0 then begin

    if FRedIndex <> -1 then begin
      LRect.Left   := 0;
      LRect.Width  := FHNameOffset;
      LRect.Top := FRedIndex*FVStep;
      LRect.Height := FVOffset;
      LStr := FNames.Strings[FRedIndex];
      FCanvas.TextRect(LRect,LStr);
    end;

    FRedIndex := AYValue div FVStep;
    LRect.Left   := 0;
    LRect.Width  := FHNameOffset;
    LRect.Top := FRedIndex*FVStep;
    LRect.Height := FVOffset;
    LStr := FNames.Strings[FRedIndex];
    FCanvas.Font.Color := clRed;
    FCanvas.TextRect(LRect,LStr);
    FCanvas.Font.Color := clBlack;
  end;
end;

end.
