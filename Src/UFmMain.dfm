object fmMain: TfmMain
  Left = 0
  Top = 0
  Caption = 'GfxLog'
  ClientHeight = 241
  ClientWidth = 687
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnMouseWheel = FormMouseWheel
  OnResize = FormResize
  DesignSize = (
    687
    241)
  PixelsPerInch = 96
  TextHeight = 12
  object PaintBox1: TPaintBox
    Left = 6
    Top = 3
    Width = 675
    Height = 188
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Anchors = [akLeft, akTop, akRight, akBottom]
    Color = clWhite
    ParentColor = False
    OnClick = PaintBox1Click
    OnPaint = PaintBox1Paint
  end
  object Label1: TLabel
    Left = 440
    Top = 219
    Width = 22
    Height = 12
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Anchors = [akLeft, akBottom]
    Caption = 'Filter'
  end
  object eFilename: TEdit
    Left = 6
    Top = 216
    Width = 200
    Height = 20
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Anchors = [akLeft, akBottom]
    TabOrder = 0
  end
  object btnLoad: TButton
    Left = 210
    Top = 216
    Width = 19
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Anchors = [akLeft, akBottom]
    Caption = '...'
    TabOrder = 1
    OnClick = btnLoadClick
  end
  object sbHorizontal: TScrollBar
    Left = 6
    Top = 196
    Width = 675
    Height = 16
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Anchors = [akLeft, akRight, akBottom]
    PageSize = 0
    TabOrder = 2
    OnChange = sbHorizontalChange
  end
  object eFilter: TEdit
    Left = 485
    Top = 216
    Width = 196
    Height = 20
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 3
    OnKeyPress = eFilterKeyPress
  end
  object cbFilterInversion: TCheckBox
    Left = 408
    Top = 216
    Width = 28
    Height = 16
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Anchors = [akLeft, akBottom]
    Caption = 'Inv'
    TabOrder = 4
    OnClick = cbFilterInversionClick
  end
  object cbCaseInsensitive: TCheckBox
    Left = 347
    Top = 219
    Width = 57
    Height = 13
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Anchors = [akLeft, akBottom]
    Caption = 'Case Ins.'
    TabOrder = 5
    OnClick = cbCaseInsensitiveClick
  end
  object cbGroup: TCheckBox
    Left = 299
    Top = 219
    Width = 48
    Height = 13
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Anchors = [akLeft, akBottom]
    Caption = 'Group'
    TabOrder = 6
    OnClick = cbGroupClick
  end
  object OpenDialog1: TOpenDialog
    Left = 248
    Top = 184
  end
end
